cc054.cooley
cc102.cooley
# Spark is now running with 2 workers:
export SPARK_STATUS_URL=http://cc054.cooley.pub.alcf.anl.gov:8000
export SPARK_MASTER_URI=spark://cc054:7077
Running: /home/thiruvat/code/spark/bin/spark-submit --master spark://cc054:7077 ./target/scala-2.10/simplemap-spark-scala-assembly-1.0.jar --generate --blocks 1024 --block_size 64 --nodes 2 --nparts 4 --cores 12 --json /home/thiruvat/logs/909789.json
generated option
RDD count 1025
RDD count after shift 1025
avg(x)=3313216.1179401716 avg(y)=-1591924.0190411177 avg(z)=830896.6778370887
